package com.jash.alipay1;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;


public class AsyncPayTask extends AsyncTask<String, Void, String> {
    private Activity activity;

    public AsyncPayTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... params) {
        String pay = new PayTask(activity).pay(params[0], true);
        return pay;
    }

    @Override
    protected void onPostExecute(String s) {
        PayResult result = new PayResult(s);
        switch (Integer.parseInt(result.getResultStatus())) {
            case 9000:
                Toast.makeText(activity, "订单支付成功", Toast.LENGTH_LONG).show();
                break;
            case 8000:
                Toast.makeText(activity, "正在处理中，支付结果未知", Toast.LENGTH_LONG).show();
                break;
            case 4000:
                Toast.makeText(activity, "订单支付失败", Toast.LENGTH_LONG).show();
                break;
            case 5000:
                Toast.makeText(activity, "重复请求", Toast.LENGTH_LONG).show();
                break;
            case 6001:
                Toast.makeText(activity, "用户中途取消", Toast.LENGTH_LONG).show();
                break;
            case 6002:
                Toast.makeText(activity, "网络连接出错", Toast.LENGTH_LONG).show();
                break;
            case 6004:
                Toast.makeText(activity, "支付结果未知", Toast.LENGTH_LONG).show();
                break;
            default:
                Toast.makeText(activity, "其它支付错误", Toast.LENGTH_LONG).show();
                break;

        }
    }
}
